---
layout: handbook-page-toc
title: "Marketing Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Marketing Finance Handbook!

## Common Links
 * [Financial Planning & Analysis (FP&A)](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/)
 * [Accounting](https://about.gitlab.com/handbook/finance/accounting/)
 * [Allocadia (Marketing Financial Planning tool)](https://about.gitlab.com/handbook/marketing/strategy-performance/allocadia/)


## Finance Business Partner Alignment

| Roll Up | Department | Name |
| -------- | ---- | -------- |
| Corporate Marketing | Awareness | @cmcclure1 |
| Corporate Marketing | Brand Design | @cmcclure1 | 
| Corporate Marketing | Corporate Events | @cmcclure1 |
| Corporate Marketing | Brand Campaigns | @cmcclure1 |
| Corporate Marketing | Communication | @cmcclure1 |
| Corporate Marketing | Community Relations | @cmcclure1 |
| Corporate Marketing | Content | @cmcclure1 |
| Corporate Marketing | Search Marketing | @cmcclure1 |
| Product Marketing | Product Marketing | @cmcclure1 |
| Revenue Marketing | Sales Development| @cmcclure1 |
| Marketing Strategy and Ops | Digital Experience | @rcallam |
| Marketing Strategy and Ops | Marketing Ops | @rcallam |
| CMO Executive | CMO Executive | @rcallam |
| Integrated Marketing | Digital Marketing | @rcallam |
| Integrated Marketing | Partner Marketing | @rcallam |
| Integrated Marketing | Campaigns | @rcallam |
| Integrated Marketing | Field Marketing | @rcallam |
| Integrated Marketing | Account Based Marketing | @rcallam |




## Marketing Variance Packages

The Marketing Variance Review meeting happens once a month to review the actual expenses for the previous month. For example, in the 3rd week of June we will review May's expenses.  

## Important Dates
* 2022-01-14: December Actuals loaded into Allocadia (estimated) 
* 2022-01-14: FY23 Targets added into Allocadia (estimated)
* 2022-01-25: January Forecast Due 
* 2022-02-14: January Actuals loaded into Allocadia (estimated)
* 2022-02-22: February Forecast Due
* 2022-03-14: February Actuals loaded into Allocadia (estimated) 
* 2022-03-24: March Forecast Due
* 2022-04-14: March Actuals loaded into Allocadia (estimated) 
* 2022-04-22: April Forecast Due
* 2022-04-29: Q2 Plan Due 
* 2022-05-13: April Actuals loaded into Allocadia (estimated) 
* 2022-05-24: May Forecast Due
* 2022-06-14: May Actuals loaded into Allocadia (estimated) 
* 2022-06-23: June Forecast Due
* 2022-07-14: June Actuals loaded into Allocadia (estimated) 
* 2022-07-22: July Forecast Due 
* 2022-07-29: Q3 Plan Due 
* 2022-08-15: July Actuals loaded into Allocadia (estimated) 
* 2022-08-24: August Forecast Due 
* 2022-09-14: August Actuals loaded into Allocadia (estimated) 
* 2022-09-23: September Forecast Due 
* 2022-10-14: September  Actuals loaded into Allocadia (estimated) 
* 2022-10-24: October Forecast Due 
* 2022-11-14: October Actuals loaded into Allocadia (estimated) 
* 2022-10-31: Q4 Plan Due 
* 2022-11-18: FY24 Bottom's Up Plan due in Allocadia (estimated)
* 2022-11-22: November Forecast Due 
* 2022-12-14: November Actuals loaded into Allocadia (estimated)
* 2022-12-22: December Forecast Due 
* 2023-01-14: December Actuals loaded into Allocadia (estimated) 
* 2023-01-14: FY24 Targets added into Allocadia (estimated)
* 2023-01-24: January Forecast Due 

## Finance Terminology

* Budget - the amount of money that is given at the start of the fiscal year
* Plan - locked at the start of the quarter and is your best itemized guess at what you will spend
* Forecast - never locked and you will continually update Allocadia through each quarter with accurate expenses as you know more

## Marketing Budget Holders

The marketing budget holder should be updating their forecast throughout the month in Allocadia as expenses occur and as they have more insight into the spend. On the Forecast Due dates listed above, Finance will take what is in Allocadia and compare that against Actuals in the Actuals vs Budget (AvB) file. When discrepancies greater than $1000 occur between forecast and actuals, accounting and finance will check with the budget holder to see if an accrual needs to occur.

At the plan due dates above, finance will take what is loaded into Allocadia for the quarter and add that into Adaptive. This is considered your plan and you will be held accountable to plan for the remainder of the quarter. Large variances from the plan will be explained at the monthly variance review meetings with CMO staff. If the department is under-spend, the difference may be reallocated to other departments. 

## Accounting Prepaid Policy

Please see the [Accounting](https://about.gitlab.com/handbook/finance/accounting/#prepaid-expense-policy) handbook page for the latest guidance on accruals and variance. As our company grows, the guidance is subject to change 

## Budget Reallocation

You must spend your budget in the same quarter that it was originally allocated. You cannot transfer budget between quarters. 

If you have budget that you do not plan to spend in the quarter, you may reallocate these dollars to another Marketing department to spend within the same fiscal quarter. When this occurs, please get CMO approval, talk with your finance business partner, and then request a budget transfer in Allocadia. 

## Sponsorships

If you bring in revenue through sponsorships during a GitLab event, please do the following: 
1. Confirm that the campaign tag for the event has an ISO date as the first eight characters. This will signal to our accounting team to recognize the sponsorships in the correct period 
2. Create an issue outlining the sponsors, sponsors contact information and the expected sponsorship amounts. Tag finance and accounting in this issue. 
3. Work with Legal to create a sponsorship contract that can be used to send out to all of sponsors. Get these contracts signed and an agreement in place.
4. Work with your accounting partner to send out invoices to the sponsors and to ensure that the revenue is being properly accounted for. 



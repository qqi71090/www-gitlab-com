---
layout: handbook-page-toc
title: Incubation Engineering Department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Incubation Engineering Department

The Incubation Engineering Department within the Engineering Division focuses on projects that are pre-Product/Market fit. The projects they work on align with the term "new markets" from one of our [product investment types](/handbook/product/investment/#investment-types). They are ideas that may contribute to our revenue in 3-5 years time. Their focus should be to move fast, ship, get feedback, and [iterate](/handbook/values/#iteration). But first they've got to get from 0 to 1 and get something shipped.

We utilize [Single-engineer Groups](/company/team/structure/#single-engineer-groups) to draw benefits, but not distractions, from within the larger company and [GitLab project](https://gitlab.com/gitlab-org/gitlab) to maintain that focus. The Single-engineer group encompasses all of product development (product management, engineering, design, and quality) at the smallest scale. They are free to learn from, and collaborate with, those larger departments at GitLab but not at the expense of slowing down unnecessarily.

The Department Head is the [VP of Incubation Engineering](/job-families/engineering/vp-of-incubation-engineering/).

### FY22 Direction

The Incubation Engineering Department is new, and the focus for FY22 is to:

* Hire team members that work as Single-Engineer Groups to deliver the departments priorities.
* Develop a repeatable process and appropriate performance indicators that allow the team to measure and demonstrate their progress and impact for each of the areas we are interested in delivering.
* Develop a scoring framework and methodology to accept and prioritise new ideas.

### Investment Framework

The aim of the SEG's in the Incubation Engineering Department is to get ideas for new markets into the Viable category from our [maturity](https://about.gitlab.com/direction/maturity/) framework.  Ideas that we successfully incubate will become a stage or feature within the relevant areas of our product, with full Product, Engineering, UX and Infrastructure support for future development.

In some cases we may invest in an SEG and then decide to acquire an existing product or team in the same area. These situations are disruptive, but unavoidable, and we will work closely with the affected SEG to adapt.

In order to determine our investment into an SEG, we use the following assumptions:

* SEGs have a 25% success rate (as opposed to 5% of VC funded companies), due to the existing market reach and technical foundations of the GitLab product and the culture, processes and support from the wider organisation.
* SEG's mature categories at a 50% faster rate than regular GitLab Category Maturity features, due to the narrow focus of our investments.
* SEG's ramp up revenue as good as the best startups in the world (5 year to $100M ARR)

Reference:

* <https://kimchihill.com/2020/04/09/path-to-100m-arr/>
* <https://www.stephnass.com/blog/saas-ipo-roadmap>

### What is a New Market?

A new market in our product roadmap is a long-term bet to help us expand our Serviceable Addressable Market (SAM) over a 3-5 year time horizon.

We look at areas that fit within our [company mission](https://about.gitlab.com/company/mission/) that we currently do not service and have one or more competitors that have validated the customer need.  Our criteria for entering these markets is that there is at least one competitor that is attaining over $100M annual recurring revenue, or at least 3 venture capital funded startups that have taken over $5M in funding.  We track our understanding of these markets via this [internal document](https://docs.google.com/document/d/17nD0Pko3Ff-ensiE0SUl3atxassZlC7pEpuIvzTy8Ek/edit?usp=sharing).

We also manage projects that may not fit the above definition, but are well suited to the [SEG](/company/team/structure/#single-engineer-groups) model that we use as they require a degree of experimentation with significant risk of failure. These are significant defensive roadmap items that are important to work on to maintain our market share and competitive advantage in an area, but we cannot yet justify funding a larger R&D effort. 

The Incubation Engineering Department is not suited to deliver regular roadmap items as they are typically smaller in effort, have less risk of failure, and have less scope for experimentation to determine product/user fit.

### Demonstrating Progress

Our key focus is to ship software, we do this [Iteratively](https://about.gitlab.com/handbook/values/#iteration) and [Transparently](https://about.gitlab.com/handbook/values/#transparency) in an Agile way, concentrating on having something that works as soon as possible that we can build upon.

For each of our Investment areas, we aim to showcase our progress on a weekly basis using the [Engineering Demo Process](https://about.gitlab.com/handbook/engineering/#engineering-demo-process).  On the tracking issue for each area, each SEG should provide a status update and a link to a video of the current functionality by the end of each week.  We should aim to develop a scorecard to help [grade](https://about.gitlab.com/handbook/engineering/#grading) our progress against each of the key features we are trying to develop, and include this in our weekly update.

We do this in order to ensure alignment with impacted teams at GitLab, to provide opportunties for [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) from interested GitLab team members, community members, and users, and finally to demonstrate [Results](https://about.gitlab.com/handbook/values/#results).

The key points to cover in each of our weekly updates are:

* *What progress have we made?* Each week we should be moving closer to our goal of having a [Viable](https://about.gitlab.com/direction/maturity/) product or feature.
* *What have we learnt?* Either through technical research, experiementation, or feedback, do we better understand the problem we are trying to solve?
* *What new decisions do we need to make?* Have we identified any new opportunities or options that need wider consultation to help address?
* *What is our next focus?* Our next demo is in a week - what do we want to showcase?

When uploading a video to our YouTube channel, we should:

* Make sure they are public
* Use a consistent naming convention - "Incubation Engineering <Section> - Weekly Demo <Long Date> - <Optional Topic>"
* Add the video to the Incubation Engineering playlist, and the playlist dedicated to each section

#### Current issues and updates

*YouTube Playlists*

* [Latest Updates](https://www.youtube.com/playlist?list=PL05JrBw4t0KrQQ6BmQGY0Ji-vBaohNOlW)
* [All Videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KqqLZP0Jue3w2hz2PR6XTHi)

*DevOps for Mobile*

* [Weekly Summary](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/7)
* [Video Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoVEdembEIySgiciCuZj7Zl)

*MLOps*

* [Weekly Summary](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/16)
* [Video Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpC6-JQy8lY4tNAZKXBaM_-)

*Five Minute Production*

* [Weekly Summary](https://gitlab.com/gitlab-org/incubation-engineering/five-minute-production/meta/-/issues/7)
* [Video Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Krf0LZbfg80yo08DW1c3C36)

*Jamstack*

* [Weekly Summary](https://gitlab.com/gitlab-org/incubation-engineering/jamstack/meta/-/issues/5)
* [Video Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrjluXzJBaHsMJAZy9lR-DV)

## OKRs

The Incubation Engineering department commits to and tracks quarterly [Objectives and Key Results](https://about.gitlab.com/company/okrs/) (OKRs). 

<iframe src="https://app.ally.io/public/YDu9pSHJapFiErx" class="dashboard-embed" height="1120" width="100%" style="border:none;"> </iframe>

## Current Focus

### [5 min prod app](/handbook/engineering/incubation/5-min-production/)

The 5 Minute Production App Group will look at our developer onboarding experience and how to make it easier for Web App developers to quickly get their application deployed to Production, and to have the re-assurance that it can scale when needed.

### [AI Assist](/handbook/engineering/incubation/ai-assist/)

The AI Assist SEG will look at how machine learning can suggest security improvements to user generated functions.

### [Attack and Breach Simulation](/handbook/engineering/incubation/attack-breach-simulation)

The Attack and Breach Simulation SEG will help users determine the most vulnerable parts of their network and suggest remedies to resolve.

### [Dependency Firewall](/handbook/engineering/incubation/dependency-firewall/)

This group will work closely with our Package team to accelerate development on the [Dependency Firewall category](https://about.gitlab.com/direction/package/#dependency-firewall).

### [DevOps for Mobile Apps](/handbook/engineering/incubation/devops-for-mobile/)

The goal of the DevOps for Mobile Apps SEG is to improve the experience for Developers targeting mobile platforms by providing targeted CI/CD capabilities and workflows that improve the experience of provisioning and deploying mobile apps.

### [DevOps for Salesforce](/handbook/engineering/incubation/devops-for-salesforce/)

The goal of the DevOps for Salesforce SEG is to integrate DevOps capabilities for our customers that develop Salesforce applications.

### [Jamstack](/handbook/engineering/incubation/jamstack/)

The Jamstack group aims to enable Front End developers to simplify their toolkit by using GitLab to manage, build, and deploy externally-facing, static websites using the Jamstack architecture.

### [MLOps](/handbook/engineering/incubation/mlops/)

The MLOps Group will be focused on enabling data teams to build, test, and deploy their machine learning models. This will be net new functionality within GitLab and will bridge the gap between DataOps teams and ML/AI within production. MLOps will provide tuning, testing, and deployment of machine learning models, including version control and partial rollout and rollback.

### [No-Code / Low-Code](/handbook/engineering/incubation/no-code/)

The No-Code / Low-Code SEG will integrate with popular low/no-code platforms as well as provide a low code experience inside of GitLab.

### [Real-time Editing of Issue Descriptions (REID)](/handbook/engineering/incubation/real-time-collaboration/)

The Real-time Editing of Issue Descriptions (REID) is gathering feedback on what merge requests and code review in GitLab may be in the future and how we can significantly decrease the cycle time, increase the efficiency of code review, and create better ways of collaborating through real-time experiences.

### [Server Runtime](/handbook/engineering/incubation/server-runtime/)

The Server Runtime SEG will explore how we can increase customer usage and company revenue by delivering a cloud hosted development environment natively within GitLab.com.

### [Service Desk](/handbook/engineering/incubation/service-desk/)

The Service Desk SEG will improve our existing Service Desk offering by integrate support requests into the standard GitLab workflow.


### Backlog

We use the [RICE Framework](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework) for prioritization of ideas.  

<table id="objectives-table-bizops">
  <tr>
    <th class="text-center">
        <h5>Reach</h5>
    </th>
    <th class="text-center">
        <h5>Impact</h5>
    </th>
    <th class="text-center">
        <h5>Confidence</h5>
    </th>
  </tr>
  <tr>
    <td>How many of our users, prospects, or customers will benefit from this feature.</td>
    <td>Revenue, risk, or cost benefits to GitLab and our customers.</td>
    <td>How well we understand the customer problem and our proposed solution.</td>
  </tr>
  <tr>
    <td>
    <ul>
    <li>10.0 = Impacts the <strong>Vast</strong> majority (~80% or greater)</li>
    <li>6.0 = Impacts a <strong>Large</strong> percentage (~50% to ~80%)</li>
    <li>3.0 = <strong>Significant</strong> reach (~25% to ~50%)</li>
    <li>1.5 = <strong>Small</strong> reach (~5% to ~25%)</li>
    <li>0.5 = <strong>Minimal</strong> reach (Less than ~5%)</li>
    </ul>
    </td>
    <td>
    <ul>
    <li>3 = <strong>Massive</strong></li>
    <li>2 = <strong>High</strong></li>
    <li>1 = <strong>Medium</strong></li>
    <li>0.5 = <strong>Low</strong></li>
    <li>0.25 = <strong>Minimal</strong></li>
    </ul>
    </td>
    <td>
    <ul>
    <li>2 = <strong>High</strong></li>
    <li>1 = <strong>Medium</strong></li>
    <li>0.5 = <strong>Low</strong></li>
    </ul>
    </td>
  </tr>
  <tr>
  <td colspan="3" class="text-center"><h5>Effort</h5></td>
  </tr>
  <tr>
  <td colspan="3">How many months before we can launch an MVP to gather feedback (1 - 3).</td>
  </tr>
  </table>

The total score is calculated by **(Reach x Impact x Confidence) / Effort**

The following table is driven by the relevant RICE labels on the issues themselves.  

If you would like to propose a Single Engineer Group please reach to our [VP of Incubation Engineering](https://gitlab.com/bmarnane).

<%= partial("direction/seg-table", :locals => { :label => "SingleEngineerGroups" }) %>

## Ask Me Anything (AMA)

An AMA was held in May 2021.  

* [Agenda](https://docs.google.com/document/d/1g-xs6kGtWRflkZv4o2LNhqIADpMGzCL7kYd3SqKHkEk/edit#)
* [Video](https://youtu.be/Kb_6GqzHsng)

## Key Performance Indicators

Incubation Engineering [Key Performance Indicators](https://about.gitlab.com/handbook/engineering/incubation/performance-indicators/#incubation-engineering-department-mr-rate)

## Management Issue Board

[Incubation Engineering Issues](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/980804?scope=all&utf8=✓&label_name[]=Engineering%20Management&label_name[]=Incubation%20Engineering%20Department)

You can also reach us on Slack at `#incubation-eng` in [Slack](https://gitlab.slack.com/archives/incubation-eng) (GitLab internal)

## Quality & Support Guidelines for Incubation Projects

- Is your feature behind a feature flag? It's preferrable that it is.
- Can your feature be visually labeled as an `incubation` or `experimental` feature without distracting / taking away from the GitLab user experience?
- Error messages  / stack traces / logs may indicate that this is an `incubation` feature.
- Before feature flag roll-out, is the feature documented in sufficient detail? Documentation is the first resource for users and support agents working with the feature.
- Pro-actively reach out and make introductions before feature roll-out / general availability.

## Converting an incubation backlog project to an active SEG

In order to maintain consistency, the process below should be followed when starting a new SEG from an incubation backlog project:

1. Create a new subgroup within the [Incubation Engineering group](https://gitlab.com/gitlab-org/incubation-engineering/). For example: [devops-for-mobile-apps](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps)
2. Create a `meta` project within that new group. For example: [mlops/meta](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta)
3. Create a weekly update issue in that new `meta` project. This issue will replace the existing backlog project issue and be used to post weekly demo recordings from the new SEG. For example: [jamstack](https://gitlab.com/gitlab-org/incubation-engineering/jamstack/meta/-/issues/5)
4. Add all labels from the original backlog issue to the issue you just created. For example: [original issue](https://gitlab.com/gitlab-org/gitlab/-/issues/329592) | [new issue](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/7)
5. Add the new project to the `seg_issues_list` in the handbook direction generator module in the handbook. See this [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/93485/diffs) for an example.
6. And lastly, close out the original backlog issue with a comment pointing to the new weekly update issue ([example](https://gitlab.com/gitlab-org/gitlab/-/issues/329592#note_676451924)).

## Shadowing Field Marketing Events

An internal discussion in the Incubation Engineering team revealed that team members would benefit from shadowing customer / prospect events. This is aligned with the FY22-Q4 Engineering OKR "Increase focus on customer results..".

Field marketing has welcomed all Incubation Engineering team members to shadow upcoming field marketing events. These events are conducted with new prospects, existing customers and individual GitLab / DevOps enthusiasts.

Incubation Engineering team members may discover issues for upcoming events, and may request an invitation by commenting in the event's issue. 

The following issue boards are to be used to discover upcoming events:

- [Field & Corporate, EMEA](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933459?label_name[]=EMEA)
- [Field & Corporate, Central Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438243?label_name[]=Central%20Europe&label_name[]=EMEA)
- [Field & Corporate, UK/I](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438265?label_name[]=UK%2FI)

Highlighting EMEA because majority of Incubation Engineers reside here. Other regions can be discovered in the Boards dropdown by searching for "Field & Corporate {region_name}".

Additionally, Field Marketing has active Slack channels where events are planned and prepped for. If useful, please consider joining:

- #emea_northern_europe_fieldmarketing
- #emea_central_europe_fieldmarketing
- #emea_southern_europe_fieldmarketing

...on Slack. Other relevant channels may be discovered via Slack search.

### New to prospect / customer conversations?

If the Incubation Engineering team member is not familiar with external conversations, the golden princple to remember is that the only thing that is expected from us is to _shadow_ the call. It is perfectly okay to say nothing and stay muted throughout the call. Introduce yourself if invited to. And help GitLab team members by keeping notes.

For those comfortable with external interactions, they may offer their services to the Field Marketing event lead by means of answering questions and interacting with the participants when the GitLab speaker requests support.
